import javafx.util.Pair;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TCPServer {
    public static void main(String[] args) throws IOException {
        // start the application at port 6655
        Server test = new Server(6655);

        // app has finished, close the server
        System.out.println("closing from main");
        test.closeServer();
    }
}

class Server {
    private ServerSocket serverApp;

    public Server(int port) {
        // create new server socket
        try {
            serverApp = new ServerSocket(port);
        } catch (IOException e) {
            System.err.println("Failed to initilize the app on port: " + port);
        }

        // listen for new clients :)
        System.out.println("Starting an app - listening on port: " + port);
        listen();
    }

    public void closeServer() throws IOException {
        System.out.println("closing the socket and quiting the app");
        serverApp.close();
    }

    private void listen() {
        // initialize id increment to assign id for each client
        int IdIncrement = 1;

        // active waiting for all new clients
        while (true) {
            try {
                // wait for new client
                Socket client = serverApp.accept();

                // create new connection with unique id and client socket
                Connection connection = new Connection(client, IdIncrement++);

                // start new thread with Connection object
                new Thread(connection).start();
            } catch (IOException e) {
                System.err.println("Failed to attach client");
            }
        }
    }

}

class Response {
    public static String SERVER_MOVE = "102 MOVE\u0007\b";
    public static String SERVER_TURN_LEFT = "103 TURN LEFT\u0007\b";
    public static String SERVER_TURN_RIGHT = "104 TURN RIGHT\u0007\b";
    public static String SERVER_PICK_UP = "105 GET MESSAGE\u0007\b";
    public static String SERVER_LOGOUT = "106 LOGOUT\u0007\b";
    public static String SERVER_OK = "200 OK\u0007\b";
    public static String SERVER_LOGIN_FAILED = "300 LOGIN FAILED\u0007\b";
    public static String SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR\u0007\b";
    public static String SERVER_LOGIC_ERROR = "302 LOGIC ERROR\u0007\b";
}

/// fail states
class LoginFailedException extends Exception {
}

class SyntaxErrorException extends Exception {
}

class LogicErrorException extends Exception {
}

/// success state
class FoundException extends Exception {
}

class Connection implements Runnable {
    private Socket client;
    PrintWriter out;
    BufferedReader in;
    int id;
    Direction direction;
    int x;
    int y;

    public Connection(Socket client, int id) {
        this.client = client;
        this.id = id;
        try {
            // set basic timeout, keepAlice and tcp nodelay
            this.client.setKeepAlive(true);
            this.client.setTcpNoDelay(true);
            this.client.setSoTimeout(1000);

            // pipe incoming and outcoming streams
            out = new PrintWriter(client.getOutputStream());
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        } catch (IOException e) {
            // print given exception
            e.printStackTrace();

            // close the connection
            close();
        }
    }

    @Override
    public void run() {
        try {
            // push control to start function
            start();
        } catch (FoundException e) {
            e.printStackTrace();
            send(Response.SERVER_LOGOUT);
            close();
        } catch (LoginFailedException e) {
            e.printStackTrace();
            send(Response.SERVER_LOGIN_FAILED);
            close();
        } catch (SyntaxErrorException e) {
            e.printStackTrace();
            send(Response.SERVER_SYNTAX_ERROR);
            close();
        } catch (LogicErrorException e) {
            e.printStackTrace();
            send(Response.SERVER_LOGIC_ERROR);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            close();
        }
    }

    private void start() throws SyntaxErrorException, LoginFailedException, IOException, LogicErrorException, FoundException, TimeoutException {
        // read name from input - blocking
        String name = read(12);

        // generate server hash
        Pair<Integer, Integer> serverHashPair = getHash(name, 54621);

        // send server hash
        send(serverHashPair.getKey() + "\u0007\b");

        // read response from input - blocking
        String clientHashString = read(12);

        int clientHash;
        try {
            // parse incoming string to int representing client hash
            clientHash = Integer.parseInt(clientHashString);
        } catch (Exception e) {
            System.err.println("hash parse error");
            throw new SyntaxErrorException();
        }

        // check if client hash overflow
        if (clientHash > 65536) {
            System.err.println("hash overflow error");
            throw new SyntaxErrorException();
        }

        // create back "check sum" hash
        int checksumHash = (serverHashPair.getValue() + 45328) % 65536;

        // check if client hash is valid
        if (checksumHash != clientHash) {
            throw new LoginFailedException();
        }

        // send server response ok
        send(Response.SERVER_OK);

        // send first command to move to determine position
        send(Response.SERVER_MOVE);

        // read response from client about initial position
        String initialPositionResponse = read(14);

        // set initial position
        Pair<Integer, Integer> initialPosition = extractPosition(initialPositionResponse);

        // set initial values
        x = initialPosition.getKey();
        y = initialPosition.getValue();

        boolean foundDirection = false;

        // move until you move to determine your direction
        while (!foundDirection) {

            // send second command to move to determine direction
            send(Response.SERVER_MOVE);

            // read response from client about new position
            String newPositionResponse = read(14);

            // set initial position
            Pair<Integer, Integer> newPosition = extractPosition(newPositionResponse);

            // get direction
            foundDirection = getDirection(x, y, newPosition.getKey(), newPosition.getValue());

            // set updated values
            x = newPosition.getKey();
            y = newPosition.getValue();
        }

        // check position in space and rotate to border line of map (x = -2)
        if (x > -3) {
            // rotate to the west
            while (direction != Direction.WEST)
                rotateRight();
        } else {
            // rotate to the east
            while (direction != Direction.EAST)
                rotateRight();
        }

        // go to left border of map
        while (x != -2)
            move();

        // check position in space and rotate to border line of map (y = -2)
        if (y > -3) {
            // rotate to south
            while (direction != Direction.SOUTH)
                rotateRight();
        } else {
            // rotate to north
            while (direction != Direction.NORTH)
                rotateRight();
        }

        // go to beginning of all time  :) -2, -2 position
        while (y != -2)
            move();

        // rotate to east
        while (direction != Direction.EAST)
            rotateRight();

        // search all the way up
        while (y < 3 && x < 3 && y > -3 && x > -3) {

            // move-and-pickup to the end of line
            while (x != 2) {
                pickup();
                move();
            }

            // pickup on last position in line
            pickup();

            // rotate to north
            while (direction != Direction.NORTH)
                rotateRight();

            // move to higher lines
            move();

            // rotate back to west
            while (direction != Direction.WEST)
                rotateRight();

            // move-and-pickup to the end of line
            while (x != -2) {
                pickup();
                move();
            }

            // pickup on last position in line
            pickup();

            // rotate to north
            while (direction != Direction.NORTH)
                rotateRight();

            // move to higher lines
            move();

            // rotate to east
            while (direction != Direction.EAST)
                rotateRight();
        }

        // if we reached end of map and did not find any item then logout and close connection
        send(Response.SERVER_LOGOUT);
        close();
    }

    private void pickup() throws IOException, SyntaxErrorException, FoundException, LogicErrorException, TimeoutException {
        System.out.println("server pickup at position " + x + ":" + y);

        // send command to pickup
        send(Response.SERVER_PICK_UP);

        // read response from input
        String pickupResult = read(100);

        // if it contains message - end the app
        if (!pickupResult.equals(""))
            throw new FoundException();

    }

    private Pair<Integer, Integer> extractPosition(String initialPositionResponse) throws SyntaxErrorException {
        Pair<Integer, Integer> result;

        // split response to array of strings
        String[] positionArray = initialPositionResponse.split(" ");

        // check if response has correct size
        if (positionArray.length >= 4 || initialPositionResponse.length() != initialPositionResponse.trim().length()) {
            System.err.println("extract position error");
            throw new SyntaxErrorException();
        }
        // parse position
        try {
            int x = Integer.parseInt(positionArray[1]);
            int y = Integer.parseInt(positionArray[2]);
            result = new Pair<>(x, y);
        } catch (Exception e) {
            System.err.println("extract position parse error");
            throw new SyntaxErrorException();
        }

        return result;
    }

    private void move() throws IOException, SyntaxErrorException, LogicErrorException, TimeoutException {
        Pair<Integer, Integer> currentPosition = new Pair<>(x, y);

        while (currentPosition.getKey() == x && currentPosition.getValue() == y) {
            // send command to move
            send(Response.SERVER_MOVE);

            // read response
            String response = read(14);

            // extract current position
            currentPosition = extractPosition(response);
        }

        // set current position
        x = currentPosition.getKey();
        y = currentPosition.getValue();
    }

    public Pair<Integer, Integer> getHash(String msg, int server_key) {
        int sum = 0;
        for (int i = 0; i < msg.length(); ++i) {
            sum += (int) msg.charAt(i);
        }
        int hash_check = ((sum * 1000) % 65536);
        return new Pair<>((hash_check + server_key) % 65536, hash_check);
    }

    enum Direction {
        NORTH,
        SOUTH,
        WEST,
        EAST
    }

    private boolean getDirection(int x, int y, int x_new, int y_new) {
        if (x - x_new != 0) {
            //  x and x_new are not same then robot has moved on axis x
            direction = x - x_new > 0 ? Direction.WEST : Direction.EAST;
        } else if (y - y_new != 0) {
            //  y and y_new are not same then robot has moved on axis y
            direction = y - y_new > 0 ? Direction.SOUTH : Direction.NORTH;
        } else {
            System.err.println("not detected a direction");
            return false;
        }
        return true;
    }

    private void rotateRight() throws LogicErrorException, IOException, SyntaxErrorException, TimeoutException {
        // send command to rotate right
        send(Response.SERVER_TURN_RIGHT);

        // wait for response
        read(14);

        // set inner state direction
        switch (direction) {
            case NORTH:
                direction = Direction.EAST;
                break;
            case SOUTH:
                direction = Direction.WEST;
                break;
            case WEST:
                direction = Direction.NORTH;
                break;
            case EAST:
                direction = Direction.SOUTH;
                break;
            default:
                System.err.println("not set direction exception");
                throw new LogicErrorException();
        }

        System.out.println("rotate to direction " + direction);
    }

    /// IO methods

    private void send(String message) {
        System.out.println("Sending message " + message);
        out.write(message);
        out.flush();
    }

    private String read(int max) throws IOException, SyntaxErrorException, LogicErrorException, TimeoutException {

        // create string builder for scaffolding incoming message
        StringBuilder builder = new StringBuilder();

        // iterate till the max size
        for (int i = 0; i < max; i++) {

            // wait for next char - blocking
            char c = (char) in.read();

            // if it is not the ending char append char into result string
            if (c != '\u0007') {
                // append char into result
                builder.append(c);
            } else {
                // wait for another char - blocking
                char next = (char) in.read();


                if (next != '\b') {

                    // append another char into result
                    builder.append(c);

                    // append another char into result
                    builder.append(next);

                    // increment counter cause we have waited for additional char
                    i++;
                } else {

                    // if it is last character break the loop
                    break;
                }
            }

            // if we did not break out from loop via break - it means that this string is too long
            // throw Syntax Error
            if (i == max - 2) {
                System.err.println("string too long error: " + builder);
                throw new SyntaxErrorException();
            }
        }

        // check if it is not Recharging message
        if (builder.toString().equals("RECHARGING")) {
            // robot has started recharging - blocking
            recharging();

            // wait for original message and return it
            return read(max);
        }

        System.out.println("client " + id + " has produced message " + builder.toString());

        // return accumulated result as string
        return builder.toString();
    }

    private void recharging() throws IOException, LogicErrorException, SyntaxErrorException, TimeoutException {
        String response = "";

        // set longer timeout cause the robot is recharging
        client.setSoTimeout(5000);

        // read another response
        response = read(12);

        // set regular timeout
        client.setSoTimeout(1000);

        // check validitity of message
        if (!response.equals("FULL POWER")) {
            System.err.println("recharge error");
            throw new LogicErrorException();
        }
    }

    private void close() {
        try {
            System.out.println("Closing all connections");

            // closing client socket
            client.close();

            // closing output client socket
            out.close();

            // closigin input client
            in.close();
            System.out.println("Closed all connections");
        } catch (Exception e) {
            System.err.println("Can't close all connections due to error " + e);
        }
    }
}